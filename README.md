# Wallpapers

A collection of wallpapers released under CC0 / Public Domain License

Une collection de fonds d'écran publiés sous license libre CC0 / Domaine Public

![](.thumbs/tn01.jpg)
![](.thumbs/tn02.jpg)
![](.thumbs/tn03.jpg)
![](.thumbs/tn04.jpg)
![](.thumbs/tn05.jpg)
![](.thumbs/tn06.jpg)
![](.thumbs/tn07.jpg)
![](.thumbs/tn08.jpg)
![](.thumbs/tn09.jpg)

## Outils utilisés pour la création

- Base basse résolution 1280x720 générées via IA : https://huggingface.co/spaces/playgroundai/playground-v2.5
- Images nettoyées (bordures, artefacts) via [GIMP](https://www.gimp.org/)
- Images upscalées via [Upscaler](https://gitlab.gnome.org/World/Upscaler)
